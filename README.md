# Group-Project-C

## About The Project

Based on the direct kinematics, inverse kinematics, trajectory generation solved for a manipulator for a pick and place problem, to write a program to connect with the manipulator and easily and do the pick and place task

## Built With

To use the program developed the following programs and libraries are required
1. MATLAB R2022b.
2. Peter Corke robotics toolbox RTB10.4
3. MATLAB Support for MinGW-w64 C/C++ Compiler 21.2.0
4. DYNAMIXEL SDK 3.7.31

## Prerequisites

1. Before running the app be sure to have all the softwares and libraries required for the project.
2. Clone (Download and Extract the Repository) 
4. Then on the device manager check for the device name(COM port number) and change DEVICENAME in the following files.
  * write_pos_vel.m
  * read_pos.m
  * torquedisable.m
  * write_pos_vel_gripper.m
  Example:
  ```sh
  DEVICENAME                  = 'COM4';
  ```
  ```COM4``` should be replaced by the user to a value that is read from the device manager.

## Usage Approach 1

1. Open MATLAB
2. Change directory to the cloned (extracted) folder location
5.Run the app by writing "```app```" on the matlab commad window
  ```sh
  app
  ```
2. Measure the (x, y, z) coordinates of the pick and place positions.
3. Adjust The pick and place position on the app using the sliders.
4. Use the run button to run the trajectory.

## Usage Approach 2
1. Open the ```trajectory.m``` file.
2. Move the end effector 5 cm above the place position.
3. Run the ```trajectory.m``` file.
4. When position reading is done ```:``` will show up
5. Then move the end effector to 5 cm above the pick position. Press any key and hit Enter
6. Now the trajoctory will be generated


## Authors
- [Usman Ahmad](https://github.com/Usman-Ahmad758)
- [Munir Fati Haji](https://github.com/munir-fati-haji)
- [David Ibrahim]
- [Iskel Fikiru Hordofa]





