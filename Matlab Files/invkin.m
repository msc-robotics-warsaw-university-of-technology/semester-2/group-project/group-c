function q = invkin(T,dh,q4previous)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The function takes the transformation matrix of the end effector, dh
%parameters of the manipulator and a value of joint angle 4 (q4previous) 
%to supress the singularity occur in Joints 4 and 6....
%The function outputs the joint angles of manipulator.....
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Taking the offset between motor and manipulator joint angles... 
zero = 1e-5;
q_off = dh(:,1)';
%The parameters' values extraction from DH-parameter matrix...
d1 = dh(1, 2);
a2 = dh(2, 3);
a3 = dh(3, 3);
d4 = dh(4, 2);
d6 = dh(6, 2);
%The values of each element of transformation i.e. rotation matrix and
%translation vector...
r11 = T(1,1); r21 = T(2,1); r31 = T(3,1);
r12 = T(1,2); r22 = T(2,2); r32 = T(3,2);
r13 = T(1,3); r23 = T(2,3); r33 = T(3,3);
px = T(1,4); py =  T(2,4); pz =  T(3,4);

%% The calculations of Joint Angles (Inverse Kinematics)

% alpha 1
if abs(py-d6*r23) < zero && abs(px-d6*r13) < zero
    error('Singularity Occured in Joint 1!')
else
    alpha1 = atan2(py-d6*r23,px-d6*r13);
end



% alpha 2
C = px*cos(alpha1) - d6*(r13*cos(alpha1) + r23*sin(alpha1)) + py*sin(alpha1);
B = -pz + d1 + d6*r33;
C2 = C.^2;
B2 = B.^2;
r = sqrt(C2 + B2);
E = (- a3^2 - d4^2 + C2 + B2 + a2^2)/(2*a2);

if r < zero
    error('Singularity Occured in Joint 2!')
else
    f = atan2(B, C);
    f_2 = acos(E ./ r);
    
    if ~isreal(f_2)
        error('Point Out of Reach!');
    end

    alpha2 = wrapToPi(f - f_2);
end


% alpha 3
alpha3 = wrapToPi(atan2(a3,d4) - atan2((sin(alpha2)*B + cos(alpha2)*C - a2),(cos(alpha2)*B - sin(alpha2)*C)));

% alpha 5
G = r13*cos(alpha1) + r23*sin(alpha1);
alpha5 = atan2(sqrt(1 - (-sin(alpha2+alpha3)*G - r33*cos(alpha2+alpha3))^2),(-sin(alpha2+alpha3)*G - r33*cos(alpha2+alpha3)));

if (abs(sin(alpha5))<zero)
    warning('Sin(alpha5) = 0! Singularity Occurred in Joints 4 and 6!')
    % alpha 4
    alpha4 = q4previous;
    % alpha 6
    alpha6 = atan2(r22*cos(alpha1) - r12*sin(alpha1),r21*cos(alpha1) - r11*sin(alpha1));
else
    % alpha 4
    alpha4 = atan2((r23*cos(alpha1)- r13*sin(alpha1)),(r33*sin(alpha2+alpha3) - cos(alpha2+alpha3)*G));
    % alpha 6
    c_1 = cos(alpha1); s_1 = sin(alpha1);
    c_2 = cos(alpha2); s_2 = sin(alpha2);
    c_3 = cos(alpha3); s_3 = sin(alpha3);
    c_4 = cos(alpha4); s_4 = sin(alpha4);
    c_5 = cos(alpha5); s_5 = sin(alpha5);

    num = r12.*(c_5.*s_1.*s_4+c_1.*(c_2.*(c_3.*c_4.*c_5-s_3.*s_5)-s_2.*(c_4.*c_5.*s_3+c_3.*s_5)))-r22.*(c_4.*c_5.*s_1.*s_2.*s_3+c_1.*c_5.*s_4+c_3.*s_1.*s_2.*s_5+c_2.*s_1.*(s_3.*s_5-c_3.*c_4.*c_5))+r32.*(s_3.*(s_2.*s_5-c_2.*c_4.*c_5)-c_3.*(c_4.*c_5.*s_2+c_2.*s_5));
    num = -num;
    den = r11.*(c_5.*s_1.*s_4+c_1.*(c_2.*(c_3.*c_4.*c_5-s_3.*s_5)-s_2.*(c_4.*c_5.*s_3+c_3.*s_5)))-r21.*(c_4.*c_5.*s_1.*s_2.*s_3+c_1.*c_5.*s_4+c_3.*s_1.*s_2.*s_5+c_2.*s_1.*(s_3.*s_5-c_3.*c_4.*c_5))+r31.*(s_3.*(s_2.*s_5-c_2.*c_4.*c_5)-c_3.*(c_4.*c_5.*s_2+c_2.*s_5));
    
    alpha6 = atan2(num, den);
end

%Putting all the angles calculated into the vector q and compensating the
%corresponding offsets mentioned earlier...
q = [alpha1, alpha2, alpha3, alpha4, alpha5, alpha6];
q = real(q) - q_off;
end