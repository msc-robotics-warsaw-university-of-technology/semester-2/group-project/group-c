function SendData2Manipulator(q,q_d)
% this function is responsible for sending the trajectory in the form of
% two vectors of positions and speed to the manipulator for the first 6
% joints, it loops through the two vectors to send the values to the
% manipulator in steps
for i=1:size(q,2)
    write_pos_vel(q(:,i),q_d(:,i));
end
end