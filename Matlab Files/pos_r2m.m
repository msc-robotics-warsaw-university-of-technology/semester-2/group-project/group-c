function pos_m = pos_r2m(pos_r,type)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This function takes the motors' rotational angles read through motors 'pos_r'
%and the type of motor 'type' and converts the rotation divisions from
%motor readable to the angles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Initially the angles are converted into degrees from radians...

%Maximum rotation angle of 'ax' motors is 300 divided into motor number range
%of 1023. The joint angle is converted into angles by multiplying the
%motor rotation by the ratio of 1023/300...

%Maximum rotation angle of 'ax' motors is 360 divided into motor number range
%of 4095. The joint angle is converted into angles by multiplying the
%motor rotation by the ratio of 4095/300...

pos_m = rad2deg(pos_r);
if type == "ax"
    pos_m = round(pos_m*(1023/300));
elseif type == "mx"
    pos_m = round(pos_m*(4095/360));
else
    disp("Enter the correct type of actuator!")
end