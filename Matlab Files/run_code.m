function run_code(px_pi,py_pi,pz_pi,px_pl,py_pl,pz_pl)
% this function moves the manipulator pick and place an object, given the
% x,y,z coordinates of both the pick and place positions

%% Manipulator Dimensions
d1 = 0.0; d4 = 0.1312; d6 = 0.026;
a2 = 0.15018; a3 = 0.0715;

%% DH Parameters
dh = [
-pi     d1 0 3*pi/2
-3*pi/2 0 a2 0;
-pi     0 a3 3*pi/2;
-2.618  d4 0 pi/2;
-1.0472  0 0 3*pi/2;
-2.618 d6 0 0;
];
q_off = dh(:,1)';
%% Reading Pick & Place Positions From the Manipulator

current_pose = read_all;
px_pi = 0.06 + px_pi/100;
py_pi = -0.065 + py_pi/100;
pz_pi = 0.08 + pz_pi/100;

q_default = [pi    pi    pi    2.618    2.618    2.618];

px_pl = 0.06 + px_pl/100;
py_pl = -0.065 + py_pl/100;
pz_pl = 0.08 + pz_pl/100;

EE_0 = dir_kin(dh,q_default);

tran = zeros(4,4);
tran1 = zeros(4,4);

tran(1:3,4) = [px_pi py_pi pz_pi];
TI = trotz(EE_0(6)) * troty(EE_0(5)) * trotx(EE_0(4)) + tran;
q_pi = invkin(TI, dh,0);

tran1(1:3,4) = [px_pl py_pl pz_pl];
TF = trotz(EE_0(6)) * troty(EE_0(5)) * trotx(EE_0(4)) + tran1;
q_pl = invkin(TF, dh,0); 
%% Trajectory Generation

% Current Position to Default Position in Joint Space
[q_def,q_d_def] = traj_gen(current_pose,q_default,dh,"joint");

% Default Position to Above Pick Position in Joint Space
[q_p,q_d_p] = traj_gen(q_default,q_pi,dh,"joint");

% Above Pick Position to Actual Pick Position in Task Space
EE_0 = dir_kin(dh,q_pi);
tran = zeros(4,4);

tran(1:3,4) = [EE_0(1) EE_0(2) (EE_0(3))-0.05];
TF = trotz(EE_0(6)) * troty(EE_0(5)) * trotx(EE_0(4)) + tran;

q_pi_t_f = invkin(TF,dh,q_p(4,end));

[q_pi_t,q_d_pi_t] = traj_gen(q_pi,q_pi_t_f,dh,"task");

%  Actual Pick Position to Above Pick Position in Task Space
[q_pi_f,q_d_pi_f] = traj_gen(q_pi_t_f,q_pi,dh,"task");

% Above Pick Position to Above Place Position in Joint Space
[q_pi_pl,q_d_pi_pl] = traj_gen(q_pi,q_pl,dh,"joint");

% Above Place Position to Actual Place Position in Task Space
EE_0 = dir_kin(dh,q_pl);
tran = zeros(4,4);

tran(1:3,4) = [EE_0(1) EE_0(2) (EE_0(3))-0.045];
TF = trotz(EE_0(6)) * troty(EE_0(5)) * trotx(EE_0(4)) + tran;

q_pl_t_f = invkin(TF,dh,q_pi_pl(4,end));

[q_pl_t,q_d_pl_t] = traj_gen(q_pl,q_pl_t_f,dh,"task");

%  Actual Place Position to Above Place Position in Task Space
[q_pl_f,q_d_pl_f] = traj_gen(q_pl_t_f,q_pl,dh,"task");

% Above Place Position to Default Position in Joint Space
[q_def2,q_d_def2] = traj_gen(q_pl,q_default,dh,"joint");

%% Visualization
q = cat(2,q_def,q_p,q_pi_t,q_pi_f,q_pi_pl,q_pl_t,q_pl_f,q_def2);
r = SerialLink(dh);
for i=1:size(q,2)
    q(:,i) = q(:,i)+q_off';
    r.plot([q(1,i) q(2,i) q(3,i) q(4,i) q(5,i) q(6,i)])
    pause(0.2)
end

%% Passing joints' angles to manipulator

SendData2Manipulator(q_def,q_d_def);
pause(0.5)

SendData2Manipulator(q_p,q_d_p);
pause(0.5)

% Opening The Gripper
write_pos_vel_gripper(700,0.1);
pause(0.05)

SendData2Manipulator(q_pi_t,q_d_pi_t);
pause(0.5)

% Closing The Gripper
write_pos_vel_gripper(50,0.1);
pause(0.05)

SendData2Manipulator(q_pi_f,q_d_pi_f);
pause(0.5)

SendData2Manipulator(q_pi_pl,q_d_pi_pl);
pause(0.5)

SendData2Manipulator(q_pl_t,q_d_pl_t);
pause(0.5)

% Opening The Gripper to Drop The Object
write_pos_vel_gripper(700,0.1);
pause(0.05)

SendData2Manipulator(q_pl_f,q_d_pl_f);
pause(0.5)

SendData2Manipulator(q_def2,q_d_def2);

end