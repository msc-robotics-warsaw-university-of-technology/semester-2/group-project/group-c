function [z1,p]=jomega(t0x)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The fucntion takes the transformation matrix and calculates the position /
%translation vector and z vector for jacobian...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% position vector which is the last column of transformation matrix
% excluding the last row element which is always 1....
p=[t0x(1,4); t0x(2,4); t0x(3,4)];
% Third vector of transformation matrix excluding the last row element...
z1=[t0x(1,3); t0x(2,3); t0x(3,3)];
end