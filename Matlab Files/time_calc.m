function tf = time_calc(q_i,q_f)
% this function calulates the times it takes to each joint to change its 
% position from q_i to q_f, and returns the maximum time out of them
v_max_rpm = [55 , 63 ,55 , 59, 59, 59];
v_max_rad = v_max_rpm./9.549297;
v_max = v_max_rad.*0.25;
a_max = 4;

tf = ((abs(q_f' - q_i'))./v_max') + (v_max'./a_max);

tf = ceil(max(tf));
end