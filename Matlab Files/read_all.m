function q = read_all
% this function reads the angles of all the joints of the manipulator and
% returns a vector of all 6 values in radians
q(1) = read_pos(1,"mx");
q(2) = read_pos(2,"mx");
q(3) = read_pos(3,"mx");
q(4) = read_pos(4,"ax");
q(5) = read_pos(5,"ax");
q(6) = read_pos(6,"ax");
end