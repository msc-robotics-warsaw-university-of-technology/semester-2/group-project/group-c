function vel_m = velconv(vel_r,type)
% this function converts the value of the velocity from radians to
% the manipulator's own units
% type: is the type of the motor in this joint, it could be "ax" or "mx"
% it returns the velocity in manipulator units
vel_m = vel_r*9.549297;
if type == "ax"
    vel_m = ceil(vel_r/(114/1023));
elseif type == "mx"
    vel_m = ceil(vel_r/(116.62/1023));

else
    disp("Enter the correct type of actuator!")
end

% the if condition is used so that if the values calculated were too low,
% the manipulator would not assume a 0 as a value, which is the maximum
% speed
if vel_m < 11
    vel_m = 11;
end

end