function terminate_all
% this function is responsible for disabling the torques in all 6
% joints of the manipulator
torquedisable(1);
torquedisable(2);
torquedisable(3);
torquedisable(4);
torquedisable(5);
torquedisable(6);
end