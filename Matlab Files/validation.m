% this file was used to validate the direct and inverse kinematics by
% comparing the values obtained from our functions and the values obtained
% from Peter Corke's robotics toolbox
clc
close all
clear

d1 = 0.15018; d4 = 0.026; d6 = 0.076;
a2 = 0.0715; a3 = 0.1312;

dh = [
0 d1 0 3*pi/2
0 0 a2 0;
0 0 a3 3*pi/2;
0 d4 0 pi/2;
0 0 0 3*pi/2;
0 d6 0 0;
];

r = SerialLink(dh);
q = [0.1 0.1 0.1 0.1 0.1 0.1];

% Direct Kinematics Validation
T_T = r.fkine(q)
EE = dir_kin(dh,q);
tran = zeros(4,4);
tran(1:3,4) = [EE(1) EE(2) EE(3)];
T_P = trotz(EE(6)) * troty(EE(5)) * trotx(EE(4)) + tran

% Inverse Kinematics Validation
I = r.ikine(T_T)
q1 = invkin(T_P,dh,0)