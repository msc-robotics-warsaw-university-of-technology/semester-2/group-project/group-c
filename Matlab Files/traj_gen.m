function [q,q_d] = traj_gen(q_0,q_f,dh,type)
% this function calculates an lspb trajectory from position q_0 to q_f in
% either task space or joint space and returns the position and velocity
% matrices for the trajectory generated

if type == "task"
    % Calculating the time
    tf = time_calc(q_0,q_f);

    % Generating time vector
    dt = 0.55;
    t = 0:dt:tf;

    % Direct kinematics
    EE_0 = dir_kin(dh,q_0);
    EE_f = dir_kin(dh,q_f);

    %LSPB Trajectory
    [p_x] = lspb(EE_0(1), EE_f(1), t);
    [p_y] = lspb(EE_0(2), EE_f(2), t);
    [p_z] = lspb(EE_0(3), EE_f(3), t);
    [p_g] = lspb(EE_0(4), EE_f(4), t);
    [p_b] = lspb(EE_0(5), EE_f(5), t);
    [p_a] = lspb(EE_0(6), EE_f(6), t);

    % Inverse Kinematics to get the values of the angles for each lspb
    % point
    tran = zeros(4,4);
    q = zeros(6,length(p_x));
    q_d = zeros(6,length(p_x));
    T = zeros(4,4,length(p_x));
    for i=1:length(p_x)
        tran(1:3,4) = [p_x(i) p_y(i) p_z(i)];
        T(:,:,i) = trotz(p_a(i)) * troty(p_b(i)) * trotx(p_g(i)) + tran;
        
        if i>1
            q(:,i) = invkin(T(:,:,i),dh,q(4,i-1));
            % Calculating the corresponding velocities
            q_d(:,i) = abs(q(:,i) - q(:,i-1))./dt;
        else
            q(:,i) = invkin(T(:,:,i),dh,q(4,1));
        end
    end
    
elseif type == "joint"
    % Calculating the time
    tf = time_calc(q_0,q_f);

    % Generating time vector
    dt = 0.95;
    t = 0:dt:tf;

    % LSPB Trajectory
    [q_1] = lspb(q_0(1), q_f(1), t);
    [q_2] = lspb(q_0(2), q_f(2), t);
    [q_3] = lspb(q_0(3), q_f(3), t);
    [q_4] = lspb(q_0(4), q_f(4), t);
    [q_5] = lspb(q_0(5), q_f(5), t);
    [q_6] = lspb(q_0(6), q_f(6), t);

    % Putting the 6 joints' trajectories together
    q = cat(1,q_1',q_2',q_3',q_4',q_5',q_6');
    q_d = zeros(6,size(q,2));

    % Calculating the corresponding velocities
    for i=1:size(q,2)
        if i>1
            q_d(:,i) = abs(q(:,i) - q(:,i-1))./dt;
        end
    end

end
end