function pos_r = pos_m2r(pos_m,type)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This function takes the joint angles calculated through inverse kinamatics
%'pos_m' and the type of motor 'type' and converts the angles into motor
%readable numbers range
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Maximum rotation angle of 'ax' motors is 300 divided into motor number range
%of 1023. The joint angle is converted into motor divisions by diving the
%joint angle by the ratio of 1023/300...

%Maximum rotation angle of 'mx' motors is 360 divided into motor number range
%of 4095. The joint angle is converted into motor divisions by diving the
%joint angle by the ratio of 4095/360...

%Afterwards the angles are converted into radians from degrees

if type == "ax"
    pos_r = round((pos_m)/(1023/300));
    pos_r = deg2rad(pos_r);
elseif type == "mx"
    pos_r = round((pos_m)/(4095/360));
    pos_r = deg2rad(pos_r);
else
    disp("Enter the correct type of actuator!")
end